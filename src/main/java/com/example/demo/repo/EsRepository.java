package com.example.demo.repo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.IndicesAdminClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.service.ELServiceImpl;
import com.example.demo.vo.UserVo;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;

@Repository
public class EsRepository {
	private static final Log LOG = LogFactory.getLog( EsRepository.class );
	
    private Client client;
    
	@Autowired
    public EsRepository(Client client) {
        this.client = client;
    }

	public IndexResponse setIndex(String index, String type, String id, Map data) throws ParseException { 
		IndexResponse response = client.prepareIndex(index, type, id)
		        .setSource( new JSONObject(data) )
		        .get();
		
		return response;
	}
	
	public UpdateResponse updateAPI(String index, String type, String id, Map data) {
		
		UpdateResponse response = client.prepareUpdate(index, type, id)
		        .setDoc(data)
		        .get();
		
		LOG.info("updateAPI nResult:" + response);
		return response;
	}
	
	public Object getAPI(String index, String type, String id) {
		GetResponse response = client.prepareGet(index, type, id)  
		        .get();
		
		return response;
	}
	
	public DeleteResponse deleteAPI(String index, String type, String id) {
		
		DeleteResponse response = client.prepareDelete(index, type, id).get();
		
		return response;
	}

	public CreateIndexResponse createIndexAPI(String index)  {
		
		IndicesAdminClient indicesAdminClient = client.admin().indices();
		
		CreateIndexResponse response = indicesAdminClient.prepareCreate(index).get();
		
		return response;
	}
	
	public DeleteIndexResponse deleteIndexAPI(String index)  {
		
		IndicesAdminClient indicesAdminClient = client.admin().indices();
		
		DeleteIndexResponse response = indicesAdminClient.prepareDelete(index).get();
		
		return response;
	}
	
	
	public BulkResponse bulkAPI(String index, String type, List datalist) {

		System.out.println("ElControllder bulkAPI index:"+ index + "/type:" + type+ "/size:"+ datalist.size());
		
		BulkRequestBuilder bulkRequest = client.prepareBulk();

		for(int nLoop = 0; nLoop < datalist.size() ; nLoop++ ) {
			bulkRequest.add(client.prepareIndex( index, type, (String) ((HashMap)(datalist.get(nLoop))).get("email") )
			        .setSource(  (HashMap)datalist.get(nLoop)  
			        )
			);
			
			System.out.println("ElControllder bulkAPI nLoop:"+ nLoop );
			System.out.println("ElControllder id :"+ (String) ((HashMap)(datalist.get(nLoop))).get("email") );
		}
		
		BulkResponse bulkResponse = bulkRequest.get();
		
		if (bulkResponse.hasFailures()) {
			System.out.println("ElControllder bulkAPI hasFailures msg:" + bulkResponse.buildFailureMessage());			
		}
		
		return bulkResponse;
	}
}
