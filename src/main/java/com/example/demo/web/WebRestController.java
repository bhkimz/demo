package com.example.demo.web;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.Build;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.ElService;
import com.example.demo.service.SqlService;
import com.example.demo.vo.UserVo; 

@RestController 
public class WebRestController {
	private static final Log LOG = LogFactory.getLog( WebRestController.class );
	 
	@Autowired
	SqlService sqlService;
	
	@Autowired
	ElService esservice;
	
	@GetMapping("/hello")
	public String hello() {
		return "HelloWorld";
	}

	@RequestMapping(path = {"/", "aaaa"}, method = RequestMethod.GET )
	public String index() {
		
		return "index";
	}
	
	@RequestMapping(path = {"/GetListUser" }, method = RequestMethod.GET )
	public List GetListUser() throws Exception {
		List list = sqlService.getListUser();
		LOG.info( list );
		return list;
	}
	
	@PostMapping(path = {"/insertUser" } )
	public UserVo insertUser( @RequestBody UserVo user ) throws Exception {
		
		LOG.info( "bodyParamMap : "+ user.toString() );
		
		sqlService.insertUser(user);
		LOG.info( "bodyParamMap inserted : "+ user.toString() );
		Map map = new HashMap<>();
		map.put("username", user.getUserName() );
		map.put("telnumber", user.getTelNumber() );
		map.put("email", user.getEmail() );
		map.put("birthday", user.getBirthDay() );
		map.put("sex", user.getSex() );
		map.put("age", user.getAge() );
		
		IndexResponse res = esservice.setIndex("mytable", "user", user.getEmail(), map);
		
		return user;
	}
	
	@RequestMapping(path = {"/getUser" }, method = RequestMethod.GET )
	public UserVo GetUser(@RequestParam Map<String, String> paramMap ) throws Exception {
		LOG.info( "paramMap : "+ paramMap );
		
		String email =  paramMap.get("Email");
		
		UserVo user = sqlService.getUser(email);
		
		return user;
	}
	
	@PostMapping("/updateUser")
	public String UpdateUser( @RequestBody UserVo user) throws Exception {
		
 
		LOG.info( "bodyParamMap : "+ user.toString() );
		sqlService.updateUser(user);
		
		Map map = new HashMap<>();
		map.put("username", user.getUserName() );
		map.put("telnumber", user.getTelNumber() );
		map.put("email", user.getEmail() );
		map.put("birthday", user.getBirthDay() );
		map.put("sex", user.getSex() );
		map.put("age", user.getAge() );
 
		UpdateResponse response = esservice.updateAPI("mytable", "user", user.getEmail(), map);
		
		return "UpdateUser";
	}
	
	
	@PostMapping("/deleteUser")
	public String DeleteUser(@RequestBody Map<String, String> paramMap) throws Exception {
		
		LOG.info( "paramMap : "+ paramMap );
		
		String email =  paramMap.get("Email");
		
		LOG.info( "paramMap : "+ email );
		
		sqlService.deleteUser(email);
		
		DeleteResponse response = esservice.deleteAPI("mytable", "user", email );
		LOG.info("ElControllder deleteAPI :"+ response.toString());
		
		return "DeleteUser";
	}
	
	@RequestMapping(path = {"/UserListToEla" }, method = RequestMethod.GET )
	public List UserListToEla() throws Exception {
		
		List list = sqlService.getListUser();
		
		BulkResponse response = esservice.bulkAPI("mytable", "user", list);
		
		LOG.info( list );
		return list;
	}
	
	
	@RequestMapping(value = "/BulkTest")
	public void Bulktest() throws Exception {
		sqlService.bulkTest();
	}
}
