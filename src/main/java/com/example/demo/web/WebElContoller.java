package com.example.demo.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.ElService;

import net.minidev.json.parser.ParseException;

@RestController
@RequestMapping("/El")
class WebElContoller {
	private static final Log LOG = LogFactory.getLog( WebElContoller.class );
	
	@Autowired
	ElService esservice;
	
	@GetMapping("/hello")
	public String hello() {
		return "ELHelloWorld";
	}

	
	@GetMapping("/inputData")
	public String inputData() throws ParseException {
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("user","kimchy");
		json.put("postDate",new Date());
		json.put("message","trying out Elasticsearch");
		
		IndexResponse res = esservice.setIndex("twitter", "tweet", "1", json);

		return "OK";
	}
	
	// item 갱신
	@GetMapping("/updateAPI")
	public String updateAPI(@RequestParam String index, @RequestParam String type, @RequestParam String id) {
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("user","kimchy222");
		json.put("postDate",new Date());
		json.put("message","trying out Elasticsearch");
		
		UpdateResponse response = esservice.updateAPI(index, type, id, json);
		LOG.info("ElControllder deleteAPI :"+ response.toString());
		
		return "OK";
	}
	
	// item get
	@GetMapping("/getAPI")
	public Object getAPI(@RequestParam String index, @RequestParam String type, @RequestParam String id) {
		LOG.info("ElControllder search Item");
		return esservice.getAPI(index, type, id);
	} 
	
	// item 삭제
	@GetMapping("/deleteAPI")
	public String deleteAPI(@RequestParam String index, @RequestParam String type, @RequestParam String id) {
		
		DeleteResponse response = esservice.deleteAPI(index, type, id);
		LOG.info("ElControllder deleteAPI :"+ response.toString());
		
		return "OK";
	}
	
	// Index 생성
	@GetMapping("/createIndexAPI")
	public String createIndexAPI(@RequestParam String index) {
		
		CreateIndexResponse response = esservice.createIndexAPI(index);
		//LOG.info("ElControllder deleteIndexAPI :"+ response.toString());
		
		return "OK";
	}
	
	// Index 삭제
	@GetMapping("/deleteIndexAPI")
	public String deleteIndexAPI(@RequestParam String index) {
		
		DeleteIndexResponse response = esservice.deleteIndexAPI( index );
		//LOG.info("ElControllder deleteIndexAPI :"+ response.toString());
		
		return "OK";
	} 
	
	
	// item 갱신
	@GetMapping("/bulkAPI")
	public String bulkAPI(@RequestParam String index, @RequestParam String type) {
		ArrayList list = new ArrayList<>();
		
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("user","kimchy222");
		json.put("postDate",new Date());
		json.put("message","trying out Elasticsearch");
		json.put("message2","2trying out Elasticsearch");
		list.add(json);
		
		Map<String, Object> json2 = new HashMap<String, Object>();
		json.put("user","222222");
		json.put("postDate",new Date());
		json.put("message","trying out Elasticsearch22222");
		json.put("message2","2trying out Elasticsearch22222");
		list.add(json2);
		BulkResponse response = esservice.bulkAPI(index, type, list);
		LOG.info("ElControllder deleteAPI :"+ response.toString());
		
		return "OK";
	} 
}
