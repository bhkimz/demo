package com.example.demo.service;

import java.util.List;
import java.util.Map;

import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateResponse;

import com.example.demo.vo.UserVo;

import net.minidev.json.parser.ParseException;

public interface ElService {

	IndexResponse setIndex(String index, String type, String id, Map user) throws ParseException;	
	UpdateResponse updateAPI(String index, String type, String id, Map data) ;
	Object getAPI(String index, String type, String id);
	DeleteResponse deleteAPI(String index, String type, String id);

	DeleteIndexResponse deleteIndexAPI(String index );
	BulkResponse bulkAPI(String index, String type, List datalist);
	CreateIndexResponse createIndexAPI(String index);
}
