package com.example.demo.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.UserDao;
import com.example.demo.util.BulkListUtil;
import com.example.demo.util.CallbackEvent;
import com.example.demo.vo.UserVo;

import lombok.Value;

@Service
public class SqlServiceImpl implements SqlService {
	 private static final Log LOG = LogFactory.getLog( SqlServiceImpl.class );

	 
	BulkListUtil bluNcmsChnl = new BulkListUtil(999, 5000, new CallbackEvent() {
		@Override
		public void callbackMethod(List<String> list) {
			LOG.info("chnl list : {}" + list.size());
		}
	});;

	@Autowired
	UserDao userDao;
	
	@Override
	public List getListUser() throws Exception {
		return userDao.getListUser();
	}

	@Override
	public void insertUser(UserVo user) throws Exception {
		int nResult = userDao.insertUser(user);
		LOG.info("insertUser nResult:" + nResult);
	}

	@Override
	public UserVo getUser(String email) throws Exception {
		return userDao.getUser(email);
	}

	@Override
	public void updateUser(UserVo user) throws Exception {
		int nResult = userDao.updateUser(user);
		LOG.info("updateUser nResult:" + nResult);
	}

	@Override
	public void deleteUser(String email) throws Exception {
		int nResult = userDao.deleteUser(email);
		
		LOG.info("deleteUser nResult:" + nResult);
	}

	@Override
	public void bulkTest() throws Exception {
		int nCount = 1;
		while(true) {
			if( nCount > 1000 ) {
				break;
			}
			bluNcmsChnl.add(String.valueOf(nCount));
			nCount++;
		}
	}

}
