package com.example.demo.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.repo.EsRepository;
import com.example.demo.vo.UserVo;

import net.minidev.json.parser.ParseException;

@Service
public class ELServiceImpl implements ElService {
	private static final Log LOG = LogFactory.getLog( ELServiceImpl.class );
	
	@Autowired
	EsRepository elrepository;
	
	public IndexResponse setIndex(String index, String type, String id, Map data) throws ParseException {
		return elrepository.setIndex(index, type, id, data);
	}

	@Override
	public UpdateResponse updateAPI(String index, String type, String id, Map data) {
		return elrepository.updateAPI(index, type, id, data);
	}
	
	@Override
	public Object getAPI(String index, String type, String id) {
		// TODO Auto-generated method stub
		return elrepository.getAPI(index, type, id);
	}

	@Override
	public DeleteResponse deleteAPI(String index, String type, String id) {
		DeleteResponse resopnse = elrepository.deleteAPI(index, type, id);
		return resopnse;
	}

	@Override
	public CreateIndexResponse createIndexAPI(String index) {
		CreateIndexResponse resopnse = null;
	    resopnse =  elrepository.createIndexAPI( index );
		return resopnse;
	}
	
	@Override
	public DeleteIndexResponse deleteIndexAPI(String index) {
		DeleteIndexResponse resopnse = null;
	    resopnse =  elrepository.deleteIndexAPI( index );
		return resopnse;
	}
	
	@Override
	public BulkResponse bulkAPI(String index, String type, List datalist) {
		// TODO Auto-generated method stub
		return elrepository.bulkAPI(index, type, datalist);
	}
}
