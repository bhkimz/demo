package com.example.demo.service;

import java.util.List;

import com.example.demo.vo.UserVo;

public interface SqlService {
	List getListUser() throws Exception;
	void insertUser(UserVo user) throws Exception;
	UserVo getUser(String email) throws Exception;
	void updateUser(UserVo user) throws Exception;
	void deleteUser(String email) throws Exception;
	void bulkTest( ) throws Exception;
}
