package com.example.demo.dao;

import java.util.List;

import com.example.demo.vo.UserVo;


public interface UserDao {

	List<?> getListUser() throws Exception;

	int insertUser(UserVo user) throws Exception;
	
	UserVo getUser(String email) throws Exception;
	
	int updateUser(UserVo user) throws Exception;

	int deleteUser(String email) throws Exception;
	
}
