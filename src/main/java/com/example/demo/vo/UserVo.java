package com.example.demo.vo;

import lombok.Data;


@Data
public class UserVo {
	private int Id;
	private String UserName; //userName
	private String TelNumber;
	private String Email;
	private String BirthDay;
	private String Sex;
	private String Age; 
	
}
