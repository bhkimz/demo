package com.example.demo.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BulkListUtil {
	
	private CallbackEvent callbackEvent;
	private List<String> list;
	private int workSize;
	private int workSecond;
	private Timer timer;
	private TimerTask timerTask;
	private String last;
	
	/**
	 * @param workSize
	 * @param workSecond
	 * @param callbackEvent
	 * list callback 이벤트 등록 및 초기화
	 */
	public BulkListUtil(int workSize, int workSecond, CallbackEvent callbackEvent){
		log.info("worksize : {}, worksec : {}",workSecond, workSize);
		this.callbackEvent = callbackEvent;
		this.workSize = workSize;
		this.workSecond = workSecond;
		
		initialize();
	}
	
	public void initialize(){
		log.info("initialize:" );
	  //Timer 초기화.
  	    if(timer == null) timer = new Timer();
        timerTask = new TimerTask() { // timer 등록
            
            @Override
            public void run() {
                log.info("work time is come!!");
                 
                doWork();
            }
        };
        
		if(list != null && list.size() == workSize+1){
			last = list.get(workSize);
			list = new ArrayList<String>();
			list.add(last);
			log.info("initial ### size: "+ list.size() );
			timer.schedule(this.timerTask, this.workSecond); // 1개가 리스트에 들어있으므로 타이머를 시작함.
			
		}else{
			log.info("initial list");
			list = new ArrayList<String>();
		}
		
	}
	
	/**
	 * @param str
	 * list 추가 및 timer 시작과 size 충족 시 callback event 실행.
	 */
	public synchronized void add(String str){
//	    log.info("add : {}",str);
		if(list != null && !list.contains(str)){
		    list.add(str);
		    log.info("list.size() : {} ", list.size());
			if(list.size() == 1){
				timer.schedule(this.timerTask, this.workSecond);
			}
			
		}
		
		if(list.size() == workSize+1){
			log.info("list size full : " + list.size());
			doWork();
		}
	}
	
	/**
	 * callback event 실행 및 timer 캔슬 & list 초기화.
	 */
	public synchronized void doWork(){
		if(list != null && !list.isEmpty()){
			log.info("doWork");
			
			if(list.size() > workSize) {
				log.info("doWork size before:" + list.subList(0, workSize).size() );
				callbackEvent.callbackMethod(list.subList(0, workSize));
				log.info("doWork size a:" + list.size() );
			}
			else
				callbackEvent.callbackMethod(list);
			timer.cancel();
			timer = null;
			timerTask = null;
			initialize();
		}
		
	}
}
